# Copyright (c) 2018 Salt Stack Formulas. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
{% from tpldir + "/map.jinja" import opensds with context %}

- name: include scenarios/lvm.yml
  include: scenarios/lvm.yml
  when: enabled_backend == "lvm"

- name: include scenarios/ceph.yml
  include: scenarios/ceph.yml
  when: enabled_backend == "ceph"

- name: include scenarios/cinder.yml
  include: scenarios/cinder.yml
  when: enabled_backend == "cinder" and use_cinder_standalone == false

- name: include scenarios/cinder_standalone.yml
  include: scenarios/cinder_standalone.yml
  when: enabled_backend == "cinder" and use_cinder_standalone == true


{% if opensds.db.driver|lower == "etcd" %}

include:
   {% if opensds.db.container_enabled %}
  - docker
  - etcd.docker.running
  {% else %}
  - etcd
  {% endif %}

{% endif %}

# -*- coding: utf-8 -*-
# vim: ft=yaml
