# Copyright (c) 2018 Noel McLoughlin. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---
  {% for service in opensds.services %}
kill {{ service }} daemon service:
  service.dead:
    - name: {{ service }}
    - sig: {{ service }}
    - unless: {{ opensds.sds.container.enabled }}
    - require_in:
      - file: clean opensds release files
  {% endfor %}

kill osds containerized services:
  docker_container.stopped:
    - containers:
      - {{ opensds.svc.let.controller.docker_img }}
      - {{ opensds.svc.dock.docker_img }}
    - onlyif: {{ opensds.svc.let.container_enabled }} || {{ opensds.svc.dock.container_enabled }}
    - require_in:
      - file: clean opensds release files

  {% for dir in [opensds.prefix, opensds.tmpdir, opensds.configdir, opensds.logdir,] %}
clean opensds release {{ dir }} files:
  file.absent:
    name: {{ dir }}
  {% endfor %}

clean opensds csi plugin if csi plugin specified:
  cmd.run:
    - name: . /etc/profile && kubectl delete -f deploy/kubernetes
    - cwd: {{ opensds.sds.prefix }}/csi
    - onlyif: test "{{ opensds.nbp.type|lower }}" == "csi"

  {% for dir in [opensds.nbp.prefix, opensds.nbp.tmpdir, opensds.nbp.flexvoldir,] %}
clean nbp {{ dir }} release files:
  file.absent:
    name: {{ dir }}
  {% endfor %}

  {% if opensds.svc.dock.backend|lower == "lvm" %}

    {% for vg in opensds.backend.lvm.vg %}
remove {{ vg }} volume group if lvm backend specified:
  lvm.vg_absent:
    - name: {{ vg }}
    {% endfor %}

    {% for pv in opensds.backend.lvm.pv %}
remove {{ pv }} physical volume if lvm backend specified:
  lvm.pv_absent:
    - name: {{ pv }}
    {% endfor %}

  {% elif opensds.svc.dock.backend|lower == "cinder" %}

stop cinder-standalone service:
  cmd.run:
    - name: docker-compose down
    - cwd: {{ opensds.backend.cinder.data_dir }}/cinder/contrib/block-box

    {% for vg in opensds.backend.cinder.vg %}
clean the {{ vg }} volume group of cinder:
  cmd.script:
    - source: salt://opensds/files/clean_lvm_vg.sh
    - context:
       cinder_volume_group: {{ vg }}
       cinder_data_dir: {{ opensds.backend.cinder.data_dir }}
    {% endfor %}

  {% endif %} {# Cinder #}
      
