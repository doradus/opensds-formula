# Copyright (c) 2018 Noel McLoughlin. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---
  {% if opensds.nbp.type|lower == "csi" %}

Configure opensds endpoint IP in opensds csi plugin:
  file.line:
    - name: {{ opensds.nbp.prefix }}/csi/deploy/kubernetes/csi-configmap-opensdsplugin.yaml
    - match: '^  opensdsendpoint'
    - mode: ensure
    - content: '  opensdsendpoint: {{ opensds.endpoint }}'
    - backup: True
    - require_in:
      - cmd: Prepare and deploy opensds csi plugin

Prepare and deploy opensds csi plugin:
  cmd.run:
    - name: . /etc/profile && kubectl create -f deploy/kubernetes
    - cwd:  {{ opensds.nbp.prefix }}/csi
    - output_loglevel: quiet 

  {% elif opensds.nbp.type|lower == "flexvolume" %}

Create flexvolume plugin directory if not existed:
  file.directory:
    - name: {{ opensds.nbp.flexvoldir }}
    - dir_mode: '0755'
    - makedirs: True
    - require_in:
      - file: Copy opensds flexvolume plugin binary file into flexvolume plugin dir

Copy opensds flexvolume plugin binary file into flexvolume plugin dir
  file.copy:
    - source: {{ opensds.nbp.prefix }}/flexvolume/opensds
    - name: {{ opensds.nbp.flexvoldir }}/opensds
    - makedirs: True
    - force: True
    - preserve: True

  {% else %}

Opensds nbp type is undefined or invalid:
  test.fail_without_changes:
    - name: The 'opensds.nbp.type' value must be 'csi' or 'flexvolume' 
    
  {% endif %}
  
